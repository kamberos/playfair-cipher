﻿using System;
using System.Text;
using System.Drawing;
using System.Collections.Generic;


namespace playfireCipher
{
    class CipherTools
    {
        public static List<string> Matrix = new List<string>();
        public static List<string> PairText = new List<string>();

        public static List<string> GetMatrixList()
        {
            return Matrix;
        }
        public static List<string> GetPairTextList()
        {
            return PairText;
        }
        public static void AlphabetMatrix(string key)
        {
            string alphabet = "abcdefghiklmnopqrstuvwxyz"; // Playfair cipher ignore q letter
           
            for (int j = 0; j < key.Length; j++)
            {
                Matrix.Add((key[j].ToString()));
                alphabet = alphabet.Replace(key[j].ToString(), string.Empty);
            }
            for (int i = 0; i < 25 - key.Length; i++)
            {
                Matrix.Add(alphabet[i].ToString());    
            }
        }
        public static void PrepareCiphering(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if (text.Length % 2 == 0 && text[i] == text[text.Length - 1])
                {

                }
                else if ((text.Length % 2) != 0 && text[i] == text[text.Length - 1])
                {
                    PairText.Add(string.Concat(text[i], "x"));
                }
                else
                {
                    PairText.Add(string.Concat(text[i], text[i + 1]));
                }
                i++;
            }
        }
        public static string Ciphering()
        {
            string[,] Array = new string[5, 5];
            int tmp = 0;
            
           //  Copy List value to 2D array
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < 5; j++)
                {
                    Array[i, j] += Matrix[tmp];
                    Console.Write(Array[i,j]);
                    tmp++;
                }
            }
            Console.WriteLine();
            for (int k = 0; k < PairText.Count; k++)
            {
                string tmp1 = PairText[k];
                string a = tmp1[0].ToString();
                string b = tmp1[1].ToString();
                Point aPosition = new Point();
                Point bPosition = new Point();

                for (int l = 0; l < 5; l++)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if(a == Array[l,i] )
                        {
                            aPosition.X = l;
                            aPosition.Y = i;
                        }
                        if(b == Array[l,i])
                        {
                            bPosition.X = l;
                            bPosition.Y = i;
                        }
                    }
                }
                if (aPosition.Y == bPosition.Y)
                {
                    a = Array[(aPosition.X + 1) % 5, aPosition.Y];
                    b = Array[(bPosition.X + 1) % 5, bPosition.Y];
                }
                else if (aPosition.X == bPosition.X)
                {
                        a = Array[aPosition.X, (aPosition.Y + 1 ) % 5];
                        b = Array[bPosition.X, (bPosition.Y + 1 ) % 5];
                }
                else
                {
                    a = Array[aPosition.X, bPosition.Y];
                    b = Array[bPosition.X ,aPosition.Y];

                }
                Console.Write("{0}{1}", a,b);
            }

            return "";
        }
       public static string PrepareText(string text)
        {
            StringBuilder preparetext = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (string.Equals(text[i].ToString(), "j".ToString()))
                {
                    preparetext.Append("i".ToString());
                }
                else
                {
                    preparetext.Append(text[i]);
                }
                
            }
            string sb = preparetext.ToString();

            return sb;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            string key = "dres";  // Need to add i == j
            string text = "fajnie";
            text = CipherTools.PrepareText(text); // Fix j -> i
            CipherTools.AlphabetMatrix(key);
            CipherTools.PrepareCiphering(text);
            List<string> a = CipherTools.GetMatrixList();
            //List<string> a = CipherTools.GetPairTextList();
            CipherTools.Ciphering();
              
        }
    }
}
